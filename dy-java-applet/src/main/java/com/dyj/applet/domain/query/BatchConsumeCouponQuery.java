package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

import java.util.List;

/**
 * 用户核销券请求值
 */
public class BatchConsumeCouponQuery extends UserInfoQuery {

    /**
     *
     */
    private String app_id;
    /**
     * 外部核销单号 选填
     */
    private String consume_out_no;
    /**
     *  选填
     */
    private Long consume_time;

    /**
     * 内部交易订单号 选填
     */
    private String order_id;
    /**
     * 内部券单号 选填
     */
    private List<String> coupon_id_list;

    public String getApp_id() {
        return app_id;
    }

    public BatchConsumeCouponQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getConsume_out_no() {
        return consume_out_no;
    }

    public BatchConsumeCouponQuery setConsume_out_no(String consume_out_no) {
        this.consume_out_no = consume_out_no;
        return this;
    }

    public Long getConsume_time() {
        return consume_time;
    }

    public BatchConsumeCouponQuery setConsume_time(Long consume_time) {
        this.consume_time = consume_time;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public BatchConsumeCouponQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public List<String> getCoupon_id_list() {
        return coupon_id_list;
    }

    public BatchConsumeCouponQuery setCoupon_id_list(List<String> coupon_id_list) {
        this.coupon_id_list = coupon_id_list;
        return this;
    }

    public static BatchConsumeCouponQueryBuilder builder(){
        return new BatchConsumeCouponQueryBuilder();
    }


    public static final class BatchConsumeCouponQueryBuilder {
        private String app_id;
        private String consume_out_no;
        private Long consume_time;
        private String order_id;
        private List<String> coupon_id_list;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private BatchConsumeCouponQueryBuilder() {
        }

        public static BatchConsumeCouponQueryBuilder aBatchConsumeCouponQuery() {
            return new BatchConsumeCouponQueryBuilder();
        }

        public BatchConsumeCouponQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public BatchConsumeCouponQueryBuilder consumeOutNo(String consumeOutNo) {
            this.consume_out_no = consumeOutNo;
            return this;
        }

        public BatchConsumeCouponQueryBuilder consumeTime(Long consumeTime) {
            this.consume_time = consumeTime;
            return this;
        }

        public BatchConsumeCouponQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public BatchConsumeCouponQueryBuilder couponIdList(List<String> couponIdList) {
            this.coupon_id_list = couponIdList;
            return this;
        }

        public BatchConsumeCouponQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public BatchConsumeCouponQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public BatchConsumeCouponQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public BatchConsumeCouponQuery build() {
            BatchConsumeCouponQuery batchConsumeCouponQuery = new BatchConsumeCouponQuery();
            batchConsumeCouponQuery.setApp_id(app_id);
            batchConsumeCouponQuery.setConsume_out_no(consume_out_no);
            batchConsumeCouponQuery.setConsume_time(consume_time);
            batchConsumeCouponQuery.setOrder_id(order_id);
            batchConsumeCouponQuery.setCoupon_id_list(coupon_id_list);
            batchConsumeCouponQuery.setOpen_id(open_id);
            batchConsumeCouponQuery.setTenantId(tenantId);
            batchConsumeCouponQuery.setClientKey(clientKey);
            return batchConsumeCouponQuery;
        }
    }
}
