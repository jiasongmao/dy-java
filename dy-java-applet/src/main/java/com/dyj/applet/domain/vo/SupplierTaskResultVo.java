package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-28 15:34
 **/
public class SupplierTaskResultVo extends BaseVo {

    private List<SupplierMatchResult> match_result_list;

    public List<SupplierMatchResult> getMatch_result_list() {
        return match_result_list;
    }

    public void setMatch_result_list(List<SupplierMatchResult> match_result_list) {
        this.match_result_list = match_result_list;
    }
}
