package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.SupplierDataDetail;
import com.dyj.applet.domain.SupplierSyncStatus;
import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-28 15:02
 **/
public class SupplierVo extends BaseVo {


    private SupplierDataDetail data_detail;

    /**
     * 数据同步结果
     */
    private SupplierSyncStatus sync_status;

    public SupplierDataDetail getData_detail() {
        return data_detail;
    }

    public void setData_detail(SupplierDataDetail data_detail) {
        this.data_detail = data_detail;
    }

    public SupplierSyncStatus getSync_status() {
        return sync_status;
    }

    public void setSync_status(SupplierSyncStatus sync_status) {
        this.sync_status = sync_status;
    }
}
