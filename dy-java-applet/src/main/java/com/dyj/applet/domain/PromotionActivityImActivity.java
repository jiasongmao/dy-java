package com.dyj.applet.domain;

/**
 * 创建营销活动-IM私域消息发放
 */
public class PromotionActivityImActivity<T> {

    /**
     * <p>指定用户领券玩法，不传表示不指定玩法</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">枚举值及含义：</li><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">0：不指定玩法</li><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">6: 分享裂变玩法，当前仅支持IM发放场景。</li></ul></ul> 选填
     */
    private Integer coupon_task_type;
    /**
     * <p>发放须知（长度20以内）</p><ul><li style="line-height: 1.6;margin-top: 0px;margin-bottom: 0px;">优惠券发放时候的描述文案，仅对主播展示，用户不可见，比如可向主播传递当前优惠券的稀有程度或口播建议</li></ul> 选填
     */
    private String notice_text;
    /**
     * <p>营销活动生效时间（即可领取开始时间），单位秒</p><ul><li>营销活动的声明周期必须为关联券模板生命周期的子集</li></ul>
     */
    private Long valid_begin_time;
    /**
     * <p>营销活动过期时间（即可领取结束时间），单位秒</p><ul><li>营销活动的声明周期必须为关联券模板生命周期的子集</li></ul>
     */
    private Long valid_end_time;
    /**
     * <p>当coupon_task_type=6时，填写结构内的字段</p> 选填
     */
    private T share_fission_config;

    public Integer getCoupon_task_type() {
        return coupon_task_type;
    }

    public PromotionActivityImActivity<T> setCoupon_task_type(Integer coupon_task_type) {
        this.coupon_task_type = coupon_task_type;
        return this;
    }

    public String getNotice_text() {
        return notice_text;
    }

    public PromotionActivityImActivity<T> setNotice_text(String notice_text) {
        this.notice_text = notice_text;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public PromotionActivityImActivity<T> setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public PromotionActivityImActivity<T> setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public T getShare_fission_config() {
        return share_fission_config;
    }

    public PromotionActivityImActivity<T> setShare_fission_config(T share_fission_config) {
        this.share_fission_config = share_fission_config;
        return this;
    }

    public static <T> CreatePromotionActivityImActivityBuilder builder(){
        return new CreatePromotionActivityImActivityBuilder();
    }

    public static final class CreatePromotionActivityImActivityBuilder<T> {
        private Integer coupon_task_type;
        private String notice_text;
        private Long valid_begin_time;
        private Long valid_end_time;
        private T share_fission_config;

        private CreatePromotionActivityImActivityBuilder() {
        }

        public CreatePromotionActivityImActivityBuilder<T> couponTaskType(Integer couponTaskType) {
            this.coupon_task_type = couponTaskType;
            return this;
        }

        public CreatePromotionActivityImActivityBuilder<T> noticeText(String noticeText) {
            this.notice_text = noticeText;
            return this;
        }

        public CreatePromotionActivityImActivityBuilder<T> validBeginTime(Long validBeginTime) {
            this.valid_begin_time = validBeginTime;
            return this;
        }

        public CreatePromotionActivityImActivityBuilder<T> validEndTime(Long validEndTime) {
            this.valid_end_time = validEndTime;
            return this;
        }

        public CreatePromotionActivityImActivityBuilder<T> shareFissionConfig(T shareFissionConfig) {
            this.share_fission_config = shareFissionConfig;
            return this;
        }

        public PromotionActivityImActivity<T> build() {
            PromotionActivityImActivity<T> promotionActivityImActivity = new PromotionActivityImActivity<>();
            promotionActivityImActivity.setCoupon_task_type(coupon_task_type);
            promotionActivityImActivity.setNotice_text(notice_text);
            promotionActivityImActivity.setValid_begin_time(valid_begin_time);
            promotionActivityImActivity.setValid_end_time(valid_end_time);
            promotionActivityImActivity.setShare_fission_config(share_fission_config);
            return promotionActivityImActivity;
        }
    }
}
